//TODO:
//RDS DATA to Struct -> more fluid experience;

#include "SI4703.h"

#define IN_EUROPE

SI4703::SI4703(){}

SI4703::SI4703(int SDIO_pin,int SCLK_pin,int reset_pin){
  resetPin = reset_pin;
  SDIO = SDIO_pin;
  SCLK = SCLK_pin;
}


//Write the current 9 control registers (0x02 to 0x07) to the Si4703
//It's a little weird, you don't write an I2C addres
//The Si4703 assumes you are writing to 0x02 first, then increments
byte SI4703::updateRegisters() {

  Wire.beginTransmission(SI4703_ADDRESS);
  //A write command automatically begins with register 0x02 so no need to send a write-to address
  //First we send the 0x02 to 0x07 control registers
  //In general, we should not write to registers 0x08 and 0x09
  for(int regSpot = 0x02 ; regSpot < 0x08 ; regSpot++) {
    byte high_byte = registers[regSpot] >> 8;
    byte low_byte = registers[regSpot] & 0x00FF;

    Wire.write(high_byte); //Upper 8 bits
    Wire.write(low_byte); //Lower 8 bits
  }

  //End this transmission
  byte ack = Wire.endTransmission();
  if(ack != 0) { //We have a problem!
    Serial.print("Write Fail:"); //No ACK!
    Serial.println(ack, DEC); //I2C error: 0 = success, 1 = data too long, 2 = rx NACK on address, 3 = rx NACK on data, 4 = other error
    return(FAIL);
  }

  return(SUCCESS);
}

//Read the entire register control set from 0x00 to 0x0F
void SI4703::readRegisters(){

  //Si4703 begins reading from register upper register of 0x0A and reads to 0x0F, then loops to 0x00.
  Wire.requestFrom(SI4703_ADDRESS, 32); //We want to read the entire register set from 0x0A to 0x09 = 32 bytes.

  //Remember, register 0x0A comes in first so we have to shuffle the array around a bit
  for(int x = 0x0A ; ; x++) { //Read in these 32 bytes
    if(x == 0x10) x = 0; //Loop back to zero
    registers[x] = Wire.read() << 8;
    registers[x] |= Wire.read();
    if(x == 0x09) break; //We're done!
  }
}

void SI4703::printRegisters() {
  //Read back the registers
  readRegisters();

  //Print the response array for debugging
  for(int x = 0 ; x < 16 ; x++) {
    sprintf(printBuffer, "Reg 0x%02X = 0x%04X", x, registers[x]);
    Serial.println(printBuffer);
  }
}

//Given a channel, tune to it
//Channel is in MHz, so 973 will tune to 97.3MHz
//Note: gotoChannel will go to illegal channels (ie, greater than 110MHz)
//It's left to the user to limit these if necessary
//Actually, during testing the Si4703 seems to be internally limiting it at 87.5. Neat.
void SI4703::gotoChannel(int newChannel){
  //Freq(MHz) = 0.200(in USA) * Channel + 87.5MHz
  //97.3 = 0.2 * Chan + 87.5
  //9.8 / 0.2 = 49
  newChannel *= 10; //973 * 10 = 9730
  newChannel -= 8750; //9730 - 8750 = 980

#ifdef IN_EUROPE
    newChannel /= 10; //980 / 10 = 98
#else
  newChannel /= 20; //980 / 20 = 49
#endif

  //These steps come from AN230 page 20 rev 0.5
  readRegisters();
  registers[CHANNEL] &= 0xFE00; //Clear out the channel bits
  registers[CHANNEL] |= newChannel; //Mask in the new channel
  registers[CHANNEL] |= (1<<TUNE); //Set the TUNE bit to start
  updateRegisters();

  //delay(60); //Wait 60ms - you can use or skip this delay

  //Poll to see if STC is set
  while(1) {
    readRegisters();
    if( (registers[STATUSRSSI] & (1<<STC)) != 0) break; //Tuning complete!
    Serial.println("Tuning");
  }

  readRegisters();
  registers[CHANNEL] &= ~(1<<TUNE); //Clear the tune after a tune has completed
  updateRegisters();

  //Wait for the si4703 to clear the STC as well
  while(1) {
    readRegisters();
    if( (registers[STATUSRSSI] & (1<<STC)) == 0) break; //Tuning complete!
    Serial.println("Waiting...");
  }
}

//Reads the current channel from READCHAN
//Returns a number like 973 for 97.3MHz
int SI4703::readChannel() {
  readRegisters();
  int channel = registers[READCHAN] & 0x03FF; //Mask out everything but the lower 10 bits

#ifdef IN_EUROPE
  //Freq(MHz) = 0.100(in Europe) * Channel + 87.5MHz
  //X = 0.1 * Chan + 87.5
  channel *= 1; //98 * 1 = 98 - I know this line is silly, but it makes the code look uniform
#else
  //Freq(MHz) = 0.200(in USA) * Channel + 87.5MHz
  //X = 0.2 * Chan + 87.5
  channel *= 2; //49 * 2 = 98
#endif

  channel += 875; //98 + 875 = 973
  return(channel);
}

String SI4703::readChannelAsString(){
  return String(float(readChannel()) / 10);
}

//Seeks out the next available station
//Returns the freq if it made it
//Returns zero if failed
byte SI4703::seek(byte seekDirection){
  readRegisters();

  //Set seek mode wrap bit
  registers[POWERCFG] |= (1<<SKMODE); //Allow wrap
  //registers[POWERCFG] &= ~(1<<SKMODE); //Disallow wrap - if you disallow wrap, you may want to tune to 87.5 first

  if(seekDirection == SEEK_DOWN) registers[POWERCFG] &= ~(1<<SEEKUP); //Seek down is the default upon reset
  else registers[POWERCFG] |= 1<<SEEKUP; //Set the bit to seek up

  registers[POWERCFG] |= (1<<SEEK); //Start seek

  updateRegisters(); //Seeking will now start

  //Poll to see if STC is set
  while(1) {
    readRegisters();
    if((registers[STATUSRSSI] & (1<<STC)) != 0) break; //Tuning complete!

    Serial.print("Trying station:");
    Serial.println(readChannel());
  }

  readRegisters();
  int valueSFBL = registers[STATUSRSSI] & (1<<SFBL); //Store the value of SFBL
  registers[POWERCFG] &= ~(1<<SEEK); //Clear the seek bit after seek has completed
  updateRegisters();

  //Wait for the si4703 to clear the STC as well
  while(1) {
    readRegisters();
    if( (registers[STATUSRSSI] & (1<<STC)) == 0) break; //Tuning complete!
    Serial.println("Waiting...");
    delay(10);
  }

  if(valueSFBL) { //The bit was set indicating we hit a band limit or failed to find a station
    Serial.println("Seek limit hit"); //Hit limit of band during seek
    return(FAIL);
  }

  Serial.println("Seek complete"); //Tuning complete!
  return(SUCCESS);
}

//To get the Si4703 inito 2-wire mode, SEN needs to be high and SDIO needs to be low after a reset
//The breakout board has SEN pulled high, but also has SDIO pulled high. Therefore, after a normal power up
//The Si4703 will be in an unknown state. RST must be controlled
void SI4703::init() {
  Serial.println("Initializing I2C and Si4703");

  pinMode(resetPin, OUTPUT);
  pinMode(SDIO, OUTPUT); //SDIO is connected to A4 for I2C
  digitalWrite(SDIO, LOW); //A low SDIO indicates a 2-wire interface
  digitalWrite(resetPin, LOW); //Put Si4703 into reset
  delay(1); //Some delays while we allow pins to settle
  digitalWrite(resetPin, HIGH); //Bring Si4703 out of reset with SDIO set to low and SEN pulled high with on-board resistor
  delay(1); //Allow Si4703 to come out of reset

  Wire.begin(); //Now that the unit is reset and I2C inteface mode, we need to begin I2C

  readRegisters(); //Read the current register set
  //registers[0x07] = 0xBC04; //Enable the oscillator, from AN230 page 9, rev 0.5 (DOES NOT WORK, wtf Silicon Labs datasheet?)
  registers[0x07] = 0x8100; //Enable the oscillator, from AN230 page 9, rev 0.61 (works)
  updateRegisters(); //Update

  delay(500); //Wait for clock to settle - from AN230 page 9

  readRegisters(); //Read the current register set
  registers[POWERCFG] = 0x4001; //Enable the IC
  //  registers[POWERCFG] |= (1<<SMUTE) | (1<<DMUTE); //Disable Mute, disable softmute
  registers[SYSCONFIG1] |= (1<<RDS); //Enable RDS

#ifdef IN_EUROPE
    registers[SYSCONFIG1] |= (1<<DE); //50kHz Europe setup
  registers[SYSCONFIG2] |= (1<<SPACE0); //100kHz channel spacing for Europe
#else
  registers[SYSCONFIG2] &= ~(1<<SPACE1 | 1<<SPACE0) ; //Force 200kHz channel spacing for USA
#endif

  registers[SYSCONFIG2] &= 0xFFF0; //Clear volume bits
  registers[SYSCONFIG2] |= 0x0001; //Set volume to lowest
  updateRegisters(); //Update

  delay(110); //Max powerup time, from datasheet page 13

}

bool SI4703::isValidAsciiBasicCharacterSet(byte rdsData){
  return rdsData >= 32 && rdsData <= 127;
}

bool SI4703::isValidRdsData() {

  byte blockErrors = (byte)((registers[STATUSRSSI] & 0x0600) >> 9); //Mask in BLERA;

  byte Ch = (registers[RDSC] & 0xFF00) >> 8;
  byte Cl = (registers[RDSC] & 0x00FF);

  byte Dh = (registers[RDSD] & 0xFF00) >> 8;
  byte Dl = (registers[RDSD] & 0x00FF);

  return blockErrors == 0 && isValidAsciiBasicCharacterSet(Dh) && isValidAsciiBasicCharacterSet(Dl) && isValidAsciiBasicCharacterSet(Ch) && isValidAsciiBasicCharacterSet(Cl);
}

bool SI4703::isValidStationNameData() {

  byte blockErrors = (byte)((registers[STATUSRSSI] & 0x0600) >> 9); //Mask in BLERA;

  byte Dh = (registers[RDSD] & 0xFF00) >> 8;
  byte Dl = (registers[RDSD] & 0x00FF);

  return blockErrors == 0 && isValidAsciiBasicCharacterSet(Dh) && isValidAsciiBasicCharacterSet(Dl);
}

bool SI4703::isRadioTextData() {
  return (registers[RDSB] >> 11) == 4 || (registers[RDSB] >> 11) == 5;
}

bool SI4703::isStationNameData() {
  return (registers[RDSB] >> 11) == 0 || (registers[RDSB] >> 11) == 1;
}

bool SI4703::isRdsAvailable(){
  readRegisters();

  if (registers[STATUSRSSI] & (1<<RDSR))
  {
    return true;
  }
  return false;
}

bool SI4703::isStereo(){
  readRegisters();

  if (registers[STATUSRSSI] & (1<<STEREO))
  {
    return true;
  }
  return false;
}

uint8_t SI4703::getRSSI(){
  readRegisters();
  uint8_t rssi = registers[STATUSRSSI] & 0x00FF;
  return rssi;
}

void SI4703::setRadioTextData(char* pointerToRadioTextData){
  // retrieve where this data sits in the RDS message
  byte positionOfData = (registers[RDSB] & 0x00FF & 0xf);
  byte characterPosition;

  byte Ch = (registers[RDSC] & 0xFF00) >> 8;
  byte Cl = (registers[RDSC] & 0x00FF);

  byte Dh = (registers[RDSD] & 0xFF00) >> 8;
  byte Dl = (registers[RDSD] & 0x00FF);

  characterPosition = positionOfData * 4;
  pointerToRadioTextData[characterPosition] = (char)Ch;

  characterPosition = positionOfData * 4 + 1;
  pointerToRadioTextData[characterPosition] = (char)Cl;

  characterPosition = positionOfData * 4 + 2;
  pointerToRadioTextData[characterPosition] = (char)Dh;

  characterPosition = positionOfData * 4 + 3;
  pointerToRadioTextData[characterPosition] = (char)Dl;
}

void SI4703::setStationNameData(char* pointerToStationNameData){
  // retrieve where this data sits in the RDS message
  byte positionOfData = (registers[RDSB] & 0x00FF & 0x3);
  byte characterPosition;

  byte Dh = (registers[RDSD] & 0xFF00) >> 8;
  byte Dl = (registers[RDSD] & 0x00FF);

  characterPosition = positionOfData * 2;
  pointerToStationNameData[characterPosition] = (char)Dh;

  characterPosition = positionOfData * 2 + 1;
  pointerToStationNameData[characterPosition] = (char)Dl;
}

String SI4703::getRadioText(){
  uint32_t end = millis() + RDS_TEXT_TIMEOUT;
  String radioText = "";
  char radioTextData[64];
  char* pointerToRadioTextData = radioTextData;

  memset(radioTextData, ' ', sizeof(radioTextData));

  while (millis() < end) {
    readRegisters();
    if(registers[STATUSRSSI] & (1<<RDSR)){
        if (isValidRdsData()){
          if (isRadioTextData()){

           // set each element of the radio text data as we get it
            setRadioTextData(pointerToRadioTextData);

            //if the radio text did not change this time then we break out of the loop
            if(radioText.compareTo(String(radioTextData)) == 0){
             return radioText;
            }
            radioText = String(radioTextData);
          }
        }
      delay(40); //Wait for the RDS bit to clear
    }
  }
  return radioText;
}

String SI4703::getStationName(){
  uint32_t end = millis() + RDS_NAME_TIMEOUT;
  String radioName = "";
  char stationName[8];
  char* pointerToStationNameData = stationName;

  memset(stationName, ' ', sizeof(stationName));

  while (millis() < end) {
    readRegisters();
    if(registers[STATUSRSSI] & (1<<RDSR)){
      if (isValidStationNameData()){
        if (isStationNameData()){
          // set each element of the radio station name as we get it
          setStationNameData(pointerToStationNameData);
        /*
          Serial.print("new: ");
          Serial.println(String(stationName));
          Serial.print("old: ");
          Serial.println(radioName);
        */
          //if the radio text did not change this time then we break out of the loop
          if (radioName.compareTo(String(stationName)) == 0){
            break;
          }
          radioName = String(stationName);
        }
      }
    }
    delay(40); //Wait for the RDS bit to clear
  }
  return radioName;
}

void SI4703::tune(bool level){
  int currentChannel = readChannel();
  if (level)
  {
    #ifdef IN_EUROPE
          currentChannel++; //Increase channel by 100kHz
    #else
          currentChannel += 2; //Increase channel by 200kHz
    #endif
  }else{
    #ifdef IN_EUROPE
          currentChannel--; //Increase channel by 100kHz
    #else
          currentChannel += 2; //Increase channel by 200kHz
    #endif
  }

  gotoChannel(currentChannel);

}

void SI4703::setGPIO1(bool level){
  if (level){
    registers[SYSCONFIG1] |= (1<<1) | (1<<0); //GPIO1 HIGH
  }else{
    registers[SYSCONFIG1] &= ~(1<<0); // GPIO1 LOW
  }
  updateRegisters();
}

void SI4703::setVolumeLevel(uint8_t volume){

  if(volume >= 16) volume = 15; //Max volume level for the chip is 15
  registers[SYSCONFIG2] &= 0xFFF0; //Clear volume bits
  registers[SYSCONFIG2] |= volume; //Set new volume
  updateRegisters(); //Update
}

uint8_t SI4703::getVolume(){
  readRegisters(); //Read the current register set
  uint8_t current_vol = registers[SYSCONFIG2] & 0x000F; //Read the current volume level
  return current_vol;
}

void SI4703::volume(bool direction){
  readRegisters(); //Read the current register set
  uint8_t current_vol = registers[SYSCONFIG2] & 0x000F; //Read the current volume level

  if (direction == UP)
  {
    if(current_vol < 16) current_vol++; //Limit max volume to 0x000F

  }else{

    if(current_vol > 0) current_vol--; //Limit max volume to 0x000F

  }
  setVolumeLevel(current_vol);
}

void SI4703::toggleMute(){
  readRegisters();
  registers[POWERCFG] ^= (1<<DMUTE); //Toggle Mute bit
  updateRegisters();
}

void SI4703::setMono(bool switchOn){
  readRegisters();
  if (switchOn) {
    registers[POWERCFG] |= (1 << SETMONO); // set force mono bit
  } else {
    registers[POWERCFG] &= ~(1 << SETMONO); // clear force mono bit
  }
  updateRegisters();
}
